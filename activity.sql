SHOW DATABASES;

USE music_db;

SELECT * FROM artists WHERE name LIKE "%D%";

SELECT * FROM songs WHERE length < 230;

SELECT albums.name AS albums_name, songs.title AS songs_title, songs.length AS songs_length FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE albums.name LIKE "%A%";

SELECT * FROM albums WHERE id <= 4 ORDER BY name DESC;

SELECT * FROM albums JOIN songs ON albums.id = songs.album_id 
ORDER BY albums.name DESC;

SELECT * FROM albums JOIN songs ON albums.id = songs.album_id 
ORDER BY songs.title ASC;